// // 1st

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // convert like this
// // [
// // {user:"aman",age:1},
// // {user:"pupu",age:21},
// // {user:"raj",age:2}
// // ]

// let result=[]
// let raj=Object.keys(data);
// for(let i=0; i<raj.length; i++){
//     // console.log(raj[i],data[raj[i]].name,data[raj[i]].roll);
// let obj={};
// obj={user:data[raj[i]].name,age:data[raj[i]].roll};
// result.push(obj)
// };
// console.log(result);

// // 2nd

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // console.log(data.first.name);

// // convert like this
// // [
// // {user:"aman",age:1,key:"first"},
// // {user:"pupu",age:21,key:"second"},
// // {user:"raj",age:2,key:"third"},
// // ]
// let result=[]
// let raj=Object.keys(data);
// for(let i=0; i<raj.length; i++){
//     // console.log(raj[i],data[raj[i]].name,data[raj[i]].roll)
//     let obj={};
//     obj={user:data[raj[i]].name,age:data[raj[i]].roll,key:raj[i]}
//     result.push(obj)
//  }
// console.log(result);

// //5th
// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // console.log(data.first.name);

// // convert like this
// // [
// // {name:"aman"},
// // {name:"pupu"},
// // {name:"raj"},
// // // ]
// let result=[]
// let raj=Object.keys(data);
// for(let i=0; i<raj.length; i++){
//     let obj={}
//     obj={name:data[raj[i]].name}
//     result.push(obj)
   
// }
// console.log(result);

// // // 6th
// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// convert like this
// [
//     { first: { user: 'aman', age: 1, key: 'first' } },
//     { second: { user: 'pupu', age: 21, key: 'second' } },
//     { third: { user: 'raj', age: 3, key: 'third' } }
//   ]
// let result=[]
// let raj=Object.keys(data)
// for(let i=0; i<raj.length; i++){
//     // console.log(raj[i])
//     let obj={};
//     obj={[raj[i]]:{user:data[raj[i]].name,age:data[raj[i]].roll,key:raj[i]}}
//     result.push(obj)
// }
// console.log(result)


// 7th

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // convert like this
// // {
// //     first: { name: 'aman', roll: 1, key: 'first' },
// //     second: { name: 'pupu', roll: 21, key: 'second' },
// //     third: { name: 'raj', roll: 3, key: 'third' }
// //   }
// let result=[]
// let raj=Object.keys(data)
// for(let i=0; i<raj.length; i++){
//     let obj={}
//     obj={user:data[raj[i]].name,age:data[raj[i]].roll,key:raj[i]}
  
//     // console.log(raj[i]+)
//     result.push(raj[i])
//     result.push(obj)
// }
// console.log(result)

// 8th
let data={
    "first":{name:"aman",roll:1},
    "second":{name:"pupu",roll:21},
    "third":{name:"raj",roll:3}
}


// convert like this
// [
//     '"first":{"name":"aman","roll":1,"key":"first"}',
//     '"second":{"name":"pupu","roll":21,"key":"second"}',
//     '"third":{"name":"raj","roll":3,"key":"third"}'
//   ]

let result=[]
let raj=Object.keys(data)
for(let i=0; i<raj.length; i++){
    let obj={}
    obj={user:data[raj[i]].name,age:data[raj[i]].roll,key:raj[i]}
    result.push(obj)
}
console.log(result)
