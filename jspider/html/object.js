// 1st

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// convert like this
// [
// {user:"aman",age:1},
// {user:"pupu",age:21},
// {user:"raj",age:2}
// ]
// ====================================================================================================
// 2nd

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// console.log(data.first.name);

// convert like this
// [
// {user:"aman",age:1,key:"first"},
// {user:"pupu",age:21,key:"second"},
// {user:"raj",age:2,key:"third"},
// // ]

// ========================================================================================================

// 3rd

// let student={
//     data:{
//       name:{
//         first:'Aman kumar',
//         second:{
//           roll:101,
//           address:{
//             district:'Madhepura',
//             state:'Bihar',
//             pin:852115
//           }
//         },
//         third:{
//           Skills:['JAVA','HTML','CSS','JAVASCRIPT','REACT','VUE','ANGULAR','MY SQL','GIT','LINUX','WEB SERVICE'],
//           Qualificaiton:'MCA',
//           Dream:'Software Developer'
//         },
//       }
//     }
//   }
//   // convert output like this.
//   // {Name:'Aman kumar',roll:'101',address:'Bihar',skills:'Java,HTML,CSS',Dream:'Software Developer'}

// =========================================================================================================

//4th 

// let std={
//     "101":{
//       name:'raj',
//       another:{
//         one:'concentrate',
//         description:'this is not hard',
//         motivation:'you can do it'
//       }
//     },
//     "102":{
//       name:'aman',
//       another:{
//         one:'concentrate1',
//         description:'task1',
//         motivation:'value1'
//       }
//     }
//   }
  
//   // convert like this.
//   // [
//   //   {
//   //     raj:{one:'concentrate',description:'this is not hard'}
//   //   },
//   // {
//   //   aman:{one:'concentrate1',description:'task1'}
//   // }
//   // ]


// =====================================================================================================
// //5th
// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // console.log(data.first.name);

// // convert like this
// // [
// // {name:"aman"},
// // {name:"pupu"},
// // {name:"raj"},
// // // ]

// ===================================================================================================

// 6th
// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // convert like this
// // [
// //     { first: { user: 'aman', age: 1, key: 'first' } },
// //     { second: { user: 'pupu', age: 21, key: 'second' } },
// //     { third: { user: 'raj', age: 3, key: 'third' } }
// //   ]

// ==================================================================================================

// 7th

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// // convert like this
// // {
// //     first: { name: 'aman', roll: 1, key: 'first' },
// //     second: { name: 'pupu', roll: 21, key: 'second' },
// //     third: { name: 'raj', roll: 3, key: 'third' }
// //   }
// =====================================================================================================
// 8th

// let data={
//     "first":{name:"aman",roll:1},
//     "second":{name:"pupu",roll:21},
//     "third":{name:"raj",roll:3}
// }

// convert like this
// [
//     '"first":{"name":"aman","roll":1,"key":"first"}',
//     '"second":{"name":"pupu","roll":21,"key":"second"}',
//     '"third":{"name":"raj","roll":3,"key":"third"}'
//   ]





